const express = require('express');
const hbs = require('hbs');
const fs = require('fs');
const path = require('path');
const app = express();
require('dotenv').config({ path: '.env' });
const bodyParser = require("body-parser");
const multer = require ("multer");

// Asegurarse de que el directorio de destino existe
const uploadDir = path.join(__dirname, 'public', 'assets');
if (!fs.existsSync(uploadDir)) {
    fs.mkdirSync(uploadDir, { recursive: true });
}

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, uploadDir);
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname));
    }
});
const upload = multer({ storage: storage });
const fileUpload = upload.single("imagen");

// Importar archivo de rutas
const router = require('./routes/public');
const routerAdmin = require('./routes/admin');

// Utilizar los routers
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/", router);
app.use("/admin", routerAdmin);

// Creación de aplicación express
app.use(express.static('public'));
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'views'));

hbs.registerPartials(path.join(__dirname, 'views', 'partials'));
hbs.registerHelper('extractYouTubeID', function(url) {
    const regex = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/\s]{11})/;
    const match = url.match(regex);
    return match ? match[1] : null;
});

const puerto = process.env.PORT || 3000;
app.listen(puerto, () => {
    console.log(`El servidor se está ejecutando en el puerto ${puerto}`);
});
