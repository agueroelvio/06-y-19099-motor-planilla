const express = require('express');
const { getALL, db } = require('../../db/conexion'); 

const TipoMedioCotroller = {

    index: async function (req, res) {
        try {
            const tipoMedioItems = await getALL(`SELECT * FROM TipoMedio WHERE esta_borrado = 0`);
            //const sql = (`SELECT *  FROM TipoMedio WHERE esta_borrado = 0`);
            const message = req.query.message || null;

            console.log("Filtros de busqueda", req.query);
            for(const prop in req.query ["s"]){
                if(req.query ["s"] [prop]) {
                console.log("prop", prop, req.query ["s"] [prop]);
                
                }
            }
           

            if (tipoMedioItems.length === 0) {
                return res.render("admin/TipoMedio/index", { message: "No hay elementos de tipo de medio", tipoMedioItems: [] });
            }

            const processedItems = tipoMedioItems.map(item => ({
                ...item,
                esta_borrado: item.esta_borrado ? 'Inactivo' : 'Activo'
            }));

            res.render("admin/TipoMedio/index", { message, tipoMedioItems: processedItems });
        } catch (error) {
            console.error(error);
            return res.status(500).render("error");
        }
    },

    create: async function (req, res) {
        const mensaje = req.query.mensaje;
        const datosForm = req.query;
        res.render('admin/TipoMedio/crearForm', {
            mensaje,
            datosForm,
        });
    },

    store: async function (req, res) {
        try {
            const { error, value } = TipoMedioStoreSchema.validate(req.body, { abortEarly: false });
            if (error) {
                const errorMessage = error.details.map(detail => detail.message).join(', ');
                const queryParams = new URLSearchParams(req.body).toString();
                return res.redirect(`/admin/TipoMedio/crear?mensaje=${encodeURIComponent(errorMessage)}&${queryParams}`);
            }

            const { tipo, descripcion, esta_borrado } = value;

            const tipoExistente = await TipoMedioModel.getByField('tipo', tipo);

            if (tipoExistente) {
                const queryParams = new URLSearchParams(req.body).toString();
                return res.redirect(`/admin/TipoMedio/crear?mensaje=El tipo ya existe&${queryParams}`);
            }

            await TipoMedioModel.create({ tipo, descripcion, esta_borrado });

            res.redirect("/admin/TipoMedio/listar?message=Tipo de Medio creado exitosamente");
        } catch (error) {
            console.error('Error al insertar en la base de datos:', error);
            res.status(500).send('Error al insertar en la base de datos');
        }
    },
    edit: function (req, res) {
        const idTipoMedio = req.params.idTipoMedio;
    
        db.get("SELECT * FROM TipoMedio WHERE id = ? ", [idTipoMedio], (err, tipoMedio) => {
            if (err) {
                console.error("Error al obtener el tipo de medio:", err);
                return res.status(500).render("error");
            }
    
            if (!tipoMedio) {
                return res.redirect('/admin/TipoMedio/listar?message=No se encontró el tipo de medio');
            }
    
            res.render("admin/TipoMedio/editForm", { tipoMedio });
        });
    },

    update: function (req, res) {
        const idTipoMedio = req.params.idTipoMedio;
        const { tipo, descripcion, esta_borrado } = req.body;
    
        if (!tipo || !descripcion || typeof esta_borrado === 'undefined') {
            console.error("Campos obligatorios faltantes:", { tipo, descripcion, esta_borrado });
            return res.redirect(`/admin/TipoMedio/edit/${idTipoMedio}?mensaje=Campos obligatorios faltantes`);
        }
    
        db.run(
            "UPDATE TipoMedio SET tipo = ?, descripcion = ?, esta_borrado = ? WHERE id = ?",
            [tipo, descripcion, esta_borrado, idTipoMedio],
            function (err) {
                if (err) {
                    console.error("Error al actualizar el tipo de medio:", err);
                    return res.redirect(`/admin/TipoMedio/edit/${idTipoMedio}?mensaje=Error al actualizar el tipo de medio`);
                }
                console.log(`Tipo de medio con ID ${idTipoMedio} actualizado.`);
                res.redirect('/admin/TipoMedio/listar?message=Tipo de medio actualizado exitosamente');
            }
        );
    },

    destroy: function (req, res) {
        const idTipoMedio = req.params.idTipoMedio;

        db.run("UPDATE TipoMedio SET esta_borrado = 1 WHERE id = ?", [idTipoMedio], function (err) {
            if (err) {
                console.error("Error al eliminar el tipo de medio:", err);
                return res.redirect('/admin/TipoMedio/listar?message=Error al eliminar el tipo de medio');
            }

            console.log(`Tipo de medio con ID ${idTipoMedio} eliminado.`);
            return res.redirect('/admin/TipoMedio/listar?message=Tipo de medio eliminado exitosamente');
        });
    }
};

module.exports = TipoMedioCotroller;

