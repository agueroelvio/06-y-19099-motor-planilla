const express = require('express');
const router = express.Router();
const hbs = require('hbs');
const multer = require('multer');
const path = require('path');

// Configuración de multer para guardar las imágenes en la carpeta correcta
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, '..', '..', 'public', 'assets'));
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname));
    }
});
const upload = multer({ storage: storage });
const fileUpload = upload.single('imagen');

// Controladores
const IntegranteController = require('../controllers/admin/IntegranteControllers');
const TipoMedioController = require('../controllers/admin/TipoMedioControllers');
const MediaController = require('../controllers/admin/MediaControllers');

hbs.registerHelper('eq', function (a, b) {
    return a == b;
});

// Rutas
router.get('/', (req, res) => {
    res.render('admin/index');
});

router.get('/Integrante/listar', IntegranteController.index);
router.get('/Integrante/crear', IntegranteController.create);
router.post('/Integrante/create', IntegranteController.store);
router.post('/Integrante/delete/:idIntegrante', IntegranteController.destroy);
router.get('/Integrante/edit/:idIntegrante', IntegranteController.edit);
router.post('/Integrante/update/:idIntegrante', IntegranteController.update);

router.get('/TipoMedio/listar', TipoMedioController.index);
router.get('/TipoMedio/crear', TipoMedioController.create);
router.post('/TipoMedio/create', TipoMedioController.store);
router.post('/TipoMedio/delete/:idTipoMedio', TipoMedioController.destroy);
router.get('/TipoMedio/edit/:idTipoMedio', TipoMedioController.edit);
router.post('/TipoMedio/update/:idTipoMedio', TipoMedioController.update);

router.get('/Media/listar', MediaController.index);
router.get('/Media/crear', MediaController.create);
router.post('/Media/create', fileUpload, MediaController.store);
router.post('/Media/delete/:idMedia', MediaController.destroy);
router.get('/Media/edit/:idMedia', MediaController.edit);
router.post('/Media/update/:idMedia', fileUpload, MediaController.update); // Asegúrate de usar fileUpload aquí también

module.exports = router;
